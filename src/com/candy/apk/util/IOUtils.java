package com.candy.apk.util;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by yantingjun on 2015/3/22.
 */
public class IOUtils {
    public static void close(Closeable closeable){
        if(closeable!=null){
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
