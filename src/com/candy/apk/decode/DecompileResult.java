package com.candy.apk.decode;

/**
 * Created by yantingjun on 2015/3/22.
 */
public class DecompileResult {
    private boolean success = false;
    private String msg = "";
    private String manifestFilePath = "";


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getManifestFilePath() {
        return manifestFilePath;
    }

    public void setManifestFilePath(String manifestFilePath) {
        this.manifestFilePath = manifestFilePath;
    }
}
