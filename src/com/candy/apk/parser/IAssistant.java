package com.candy.apk.parser;

import com.candy.apk.domain.IApkInfo;
import com.candy.apk.exception.ApkParseException;
import com.candy.apk.resolver.ManifestParseHandler;

/**
 * Created by yantingjun on 2015/3/22.
 */
public abstract class IAssistant {

    public String tempPath = "";
    public ManifestParseHandler manifestParseHandler;
    public abstract IApkInfo getApkInfo(String apkFile) throws ApkParseException;

    public String getTempPath() {
        return tempPath;
    }

    public void setTempPath(String tempPath) {
        this.tempPath = tempPath;
    }

    public ManifestParseHandler getManifestParseHandler() {
        return manifestParseHandler;
    }

    public void setManifestParseHandler(ManifestParseHandler manifestParseHandler) {
        this.manifestParseHandler = manifestParseHandler;
    }
}
