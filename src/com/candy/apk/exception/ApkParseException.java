package com.candy.apk.exception;

/**
 * Created by yantingjun on 2015/3/22.
 */
public class ApkParseException extends Exception{
    public ApkParseException(String msg){
        super(msg);
    }
}
